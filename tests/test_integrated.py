"""Integration test."""
from sathealth import SatHealth


def test_integrated(capsys):
    sathealth = SatHealth()
    sathealth.process('testdata.txt')
    captured = capsys.readouterr()
    assert captured.out == """[
    {
        "satelliteId": 1000,
        "severity": "RED HIGH",
        "component": "TSTAT",
        "timestamp": "2018-01-01T23:01:38.001Z"
    },
    {
        "satelliteId": 1000,
        "severity": "RED LOW",
        "component": "BATT",
        "timestamp": "2018-01-01T23:01:09.521Z"
    }
]
"""
