"""Unit tests."""
from sathealth import SatHealth
import datetime


def test_init():
    sathealth = SatHealth()
    assert sathealth.indent == 4
    assert sathealth.event_count == 3
    assert sathealth.interval_seconds == 300


def test_emit(capsys):
    sathealth = SatHealth()
    assert sathealth.emitted == 0
    sathealth._emit(satellite_id=1234,
                    severity="RED HIGH",
                    component="TSTAT",
                    timestamp=datetime.datetime(2018, 1, 1, 23, 1, 38, 1000))
    captured = capsys.readouterr()
    assert captured.out == """    {
        "satelliteId": 1234,
        "severity": "RED HIGH",
        "component": "TSTAT",
        "timestamp": "2018-01-01T23:01:38.001Z"
    }"""
    assert sathealth.emitted == 1
