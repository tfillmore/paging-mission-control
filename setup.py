import setuptools

setuptools.setup(
    name="sathealth",
    version="0.1",
    author="Tristan Fillmore",
    author_email="tristan.fillmore@live.com",
    description="Ingest status telemetry data and create alert messages",
    url="https://gitlab.com/tfillmore/paging-mission-control",
    packages=setuptools.find_packages(),
)
