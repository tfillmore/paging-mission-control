# sathealth

Ingests satellite status telemetry data and creates alert messages for the following violation conditions:

- If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.
- If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.

### Compatible Python versions

This module requires Python 3. It has been tested with Python 3.6 on Windows and Python 3.7 on Linux.

### Usage

Once the module has been pip installed, it can be invoked as follows:

`python3 -m sathealth filename`

where `filename` is the name of a telemetry data file to process


### Input Format
The input file is an ASCII text file containing pipe delimited records.

The telemetry data has the format:

```
<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
```

**WARNING:** the module assumes that input files are correctly formatted.

### Output Format
The output is a JSON array of alert message objects.  The alert messages have the following structure:

```javascript
{
    "satelliteId": 1234,
    "severity": "severity",
    "component": "component",
    "timestamp": "timestamp"
}
```

**NOTE:** this structure differs slightly from the original specification: it lints as valid JSON.


## Security Considerations
Because the output of this module is a JSON array it is susceptible to JSON hijacking and **must not** be used as the raw input to a web client.

For more information see https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/AJAX_Security_Cheat_Sheet.md
