"""This is the core of the sathealth module where the SatHealth class is defined."""
import datetime
import dateutil.parser
import json


class SatHealth:
    """This class implements a satellite health checker.

    It ingests status telemetry data and creates alert messages for the following violation conditions:

    If for the same satellite there are three battery ("BATT") voltage readings that are under the red
        low limit within a five minute interval.
    If for the same satellite there are three thermostat ("TSTAT") readings that exceed the red high
        limit within a five minute interval.
    """

    def __init__(self):
        """Initialize values."""
        self.emitted = 0
        # TODO: These values could be defined as parameters or in a configuration file
        self.indent = 4
        self.event_count = 3
        self.interval_seconds = 300

    def _emit(self, satellite_id, severity, component, timestamp):
        """Emit an elert message as a JSON object (dictionary).

        Args:
           satellite_id (int):  The satellite which is experiencing an alert condition.
           severity (str):  The alert type (RED/YELLOW) and condition (HIGH/LOW); e.g. "RED HIGH".
           component (str):  The component (TSTAT/BATT) which is experiencing the condition.
           timestamp (datetime):  The timestamp at which the condition was first experienced.
        """
        if self.emitted > 0:
            print(',')
        dump = json.dumps({'satelliteId': satellite_id,
                           'severity': severity,
                           'component': component,
                           'timestamp': timestamp.isoformat(sep='T', timespec='milliseconds') + 'Z'},
                          indent=self.indent)
        # Prepend each line with an indent
        dump = '\n'.join((' ' * self.indent + line for line in dump.split('\n')))
        print(dump, end='')
        self.emitted += 1

    def process(self, inputfile):
        """Main processing loop.

        Args:
           inputfile (str):  The telemetry data file to process.
                The telemetry data has the format:
                ```
                <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>\
                |<red-low-limit>|<raw-value>|<component>
                ```

        """
        self.emitted = 0
        status = {}
        with open(inputfile, 'r') as f:
            print('[')
            for line in f:  # TODO: This could be refactored as a generator function
                row = line.rstrip().split('|')
                timestamp = dateutil.parser.parse(row[0])
                satellite_id = int(row[1])
                red_high_limit = float(row[2])
                red_low_limit = float(row[5])
                raw_value = float(row[6])
                component = row[7]

                severity = None
                if component == 'BATT' and raw_value < red_low_limit:
                    severity = 'RED LOW'
                elif component == 'TSTAT' and raw_value > red_high_limit:
                    severity = 'RED HIGH'
                if severity:
                    if satellite_id not in status:
                        status[satellite_id] = {}
                    if severity not in status[satellite_id]:
                        status[satellite_id][severity] = set()
                    events = status[satellite_id][severity]
                    events.add(timestamp)

                    while max(events) > (min(events) + datetime.timedelta(seconds=self.interval_seconds)):
                        events.remove(min(events))

                    if len(events) >= self.event_count:
                        self._emit(satellite_id=satellite_id,
                                   severity=severity,
                                   component=component,
                                   timestamp=min(events))
                        events.clear()
            print('\n]')
