from sathealth import SatHealth
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Ingest status telemetry data and create alert messages.')
    parser.prog = 'python3 -m sathealth'
    parser.add_argument('filename', help='a telemetry data file to process')
    args = parser.parse_args()
    sathealth = SatHealth()
    sathealth.process(args.filename)
